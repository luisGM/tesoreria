/*drop database sistema_tesoreria;*/

create database sistema_tesoreria;

use sistema_tesoreria;

create table usuarios (
	id_usuario int not null primary key auto_increment,
    nombre varchar(25) not null,
    apellido varchar(25) not null,
    usuario varchar(25) not null,
    correo varchar(50) not null,
    telefono varchar(11) not null,
    direccion varchar(250) not null,
    imagen int not null,
    contra varchar(50) not null,
    tipo int not null/*0 = administrador, 1 = empleado*/
) Engine InnoDB;

create table afiliaciones (
	id_afiliacion int not null primary key auto_increment,
    nombre varchar(100) not null,
    ubicacion varchar(250) not null,
    telefono varchar(11) not null,
    fecha varchar(20) not null,
    id_usuario int not null,
    constraint foreign key (id_usuario) references usuarios(id_usuario)
) Engine InnoDB;

create table cuentas (
	id_cuenta int not null primary key auto_increment,
    numero varchar(19) not null,
    saldo double(10, 2) not null,
    id_afiliacion int not null,
    id_usuario int not null,
    constraint foreign key (id_afiliacion) references afiliaciones(id_afiliacion),
    constraint foreign key (id_usuario) references usuarios(id_usuario)
) Engine InnoDB;

create table solicitudes (
	id_solicitud int not null primary key auto_increment,
    monto double(10, 2) not null,
    fecha varchar(20) not null,
    tipo varchar(15) not null,/* 0 = mas(recaudacion), 1 = menos(erogacion) */
    estado int not null,/* 1 = aceptada, 2 = rechazada, 0 = nueva(no se ha aceptado ni rechazado aun) */
    motivo varchar(500) not null,
    situacion varchar(1000) not null,
    id_cuenta int not null,
    id_usuario int not null,
    constraint foreign key (id_usuario) references usuarios(id_usuario),
    constraint foreign key (id_cuenta) references cuentas(id_cuenta)
) Engine InnoDB;

create table bitacoras (
	id_bitacora int not null primary key auto_increment,
    informacion varchar(5000) not null,
    fecha_hora varchar(20) not null
) Engine InnoDB;    

delimiter $
	create procedure pr_hacer_solicitud (in mont double(10, 2), in tip int, in motiv varchar(500), in cuen int, in usua int)
    begin
		declare fech varchar(20);
		declare situa varchar(1000);
        declare ti varchar(15);
        declare total double(10, 2);
		declare usua_nombr varchar(25);
        declare usua_apell varchar(25);
		declare usua_corre varchar(50);
        declare info varchar(5000);
        declare cuen_numero varchar(19);
        declare cuen_afilia varchar(25);
        declare cuen_saldo double(10, 2);
        set fech = now();
        if tip = 1 then
			set total = (select saldo from cuentas where id_cuenta in (cuen)) - mont;
		else
			set total = (select saldo from cuentas where id_cuenta in (cuen)) + mont;
        end if;
        if total < 0 then
			if tip = 0 then
				set situa = concat("El saldo de la cuenta aumentara a: $", total, " dolares");
            else
				set situa = concat("El saldo en la cuenta es insuficiente para concebir esta cantidad, faltarian $", (total*-1), " dolares");
            end if;
        elseif total = 0 then
			if tip = 0 then
				set situa = concat("El saldo de la cuenta aumentara a: $", total, " dolares");
            else
				set situa = "El saldo en la cuenta quedaria a $0 si acepta la solicitud";
            end if;
		else
			if tip = 0 then
				set situa = concat("El saldo de la cuenta aumentara a: $", (((total*-1) + mont) + mont), " dolares");
            else
				set situa = concat("Si acepta esta solicitud, en la cuenta quedarian $", (total*-1), " dolares");
            end if;
        end if;
        if tip = 0 then
			set ti = "recaudacion";
        else
			set ti = "erogacion";
        end if;
        insert into solicitudes(monto, fecha, tipo, estado, motivo, situacion, id_cuenta, id_usuario) values
			(mont, fech, ti, 0, motiv, situa, cuen, usua);
		set usua_nombr = (select nombre from usuarios where id_usuario in (usua));
        set usua_apell = (select apellido from usuarios where id_usuario in (usua));
        set usua_corre = (select correo from usuarios where id_usuario in (usua));
        set cuen_afilia = (select a.nombre from cuentas as c inner join afiliaciones as a on a.id_afiliacion = c.id_afiliacion where c.id_cuenta in (cuen));
        set cuen_saldo = (select saldo from cuentas where id_cuenta in (cuen));
        set cuen_numero = (select numero from cuentas where id_cuenta in (cuen));
        set info = concat("Se<span style='color:blue;font-weight:bold;'> solicito </span> una",
			"<span style='color:blue;font-weight:bold;'> ", ti, " </span> de $",
			"<span style='color:blue;font-weight:bold;'> ", mont, " </span> dolares, en la fecha",
			"<span style='color:blue;font-weight:bold;'> ", fech, " </span>. De el numero de cuenta: #",
			"<span style='color:blue;font-weight:bold;'> ", cuen_numero, " </span>perteneciente a la afiliacion nombrada: ",
            "<span style='color:blue;font-weight:bold;'> ", cuen_afilia, " </span>. Fue creada por el usuario",
			"<span style='color:blue;font-weight:bold;'> ", usua_nombr, " ", usua_apell, " </span> con el correo: ",
			"<span style='color:blue;font-weight:bold;'> ", usua_corre, " </span>. Su situacion financiera en este momento fue: '",
			"<span style='color:blue;font-weight:bold;'> ", situa, " </span>'.");
		insert into bitacoras (informacion, fecha_hora) values (info, now());
    end $
delimiter ;
    
delimiter $
	create procedure pr_crear_afiliacion (in nom varchar(100), in ubi varchar(250), in tele varchar(11), in usua int)
    begin 
		declare info varchar(5000);
		declare usua_nombr varchar(25);
        declare usua_apell varchar(25);
		declare usua_corre varchar(50);
		insert into afiliaciones (nombre, ubicacion, telefono, fecha, id_usuario) values 
			(nom, ubi, tele, now(), usua);
		set usua_nombr = (select nombre from usuarios where id_usuario in (usua));
        set usua_apell = (select apellido from usuarios where id_usuario in (usua));
        set usua_corre = (select correo from usuarios where id_usuario in (usua));
        set info = concat("Ha sido<span style='color:orange;font-weight:bold;'> insertada </span> una nueva",
			"<span style='color:orange;font-weight:bold;'> Afiliacion </span> con el nombre de #",
            "<span style='color:orange;font-weight:bold;'> ", nom, " </span>, ubicada en: ",
            "<span style='color:orange;font-weight:bold;'> ", ubi, " </span>, con el numero de telefono #",
            "<span style='color:orange;font-weight:bold;'> ", tele, " </span>. Fue ingresada por el usuario",
            "<span style='color:orange;font-weight:bold;'> ", usua_nombr, " ", usua_apell, " </span>, propietario del correo",
            "<span style='color:orange;font-weight:bold;'> ", usua_corre, " </span>.");
		insert into bitacoras (informacion, fecha_hora) values (info, now());
    end $
delimiter ;
    
delimiter $
	create procedure pr_crear_cuentas (in num varchar(19), in afil int, in usua int)
		begin
            declare info varchar(5000);
            declare afil_nom varchar(100);
            declare afil_ubic varchar(250);
            declare afil_tele varchar(11);
            declare afil_fecha varchar(20);
            declare usua_nombr varchar(25);
            declare usua_apell varchar(25);
            declare usua_corre varchar(50);
			insert into cuentas (numero, saldo, id_afiliacion, id_usuario) values (num, 0.00, afil, usua);
            set afil_nom = (select nombre from afiliaciones where id_afiliacion in (afil));
            set afil_ubic = (select ubicacion from afiliaciones where id_afiliacion in (afil));
            set afil_tele = (select telefono from afiliaciones where id_afiliacion in (afil));
            set afil_fecha = (select fecha from afiliaciones where id_afiliacion in (afil));
            set usua_nombr = (select nombre from usuarios where id_usuario in (usua));
            set usua_apell = (select apellido from usuarios where id_usuario in (usua));
            set usua_corre = (select correo from usuarios where id_usuario in (usua));
            set info = concat("Se ha <span style='color:crimson;font-weight:bold;'>insertado</span> una nueva <span style='color",
				":crimson;font-weight:bold;'>cuenta</span> con el numero de <span style='color:crimson;font-weight:bold;'>#", 
				num, "</span> relacionada con la afiliacion: <span style='color:crimson;font-weight:bold;'>", afil_nom, "</span>", 
				", la que se ingreso en la fecha:<span style='color:crimson;font-weight:bold;'> >>", afil_fecha, 
                "<< </span>, en la ubiacacion: <span style='color:crimson;font-weight:bold;'>", afil_ubic, "</span>",
                ", con numero de telefono <span style='color:crimson;font-weight:bold;'>#", afil_tele, "</span>",
                ". EL numero de cuenta fue ingresada por el usuario", 
                "<span style='color:crimson;font-weight:bold;'> ", usua_nombr, " ", usua_apell, " </span>", 
                "con el correo asignado a: <span style='color:crimson;font-weight:bold;'> ", usua_corre, " </span>.");
            insert into bitacoras (informacion, fecha_hora) values (info, now());
		end $
delimiter ;

delimiter $
	create procedure pr_crear_usuario (in nom varchar(25), in apel varchar(25), in usu varchar(25), 
			in corr varchar(50), in tele varchar(11), in dire varchar(250), in imag int, 
            in cont varchar(50), in ti int)
		begin
			declare info varchar(5000);
			insert into usuarios (nombre, apellido, usuario, correo, telefono, direccion, imagen, contra, tipo) values
				(nom, apel, usu, corr, tele, dire, imag, cont, ti);
			set info = concat("Se ha <span style='color:purple;font-weight:bold;'> insertado </span> un nuevo ", 
				"<span style='color:purple;font-weight:bold;'> usuario </span> con el correo ", 
                "<span style='color:purple;font-weight:bold;'> ", corr, " </span>, conocido como",
                "<span style='color:purple;font-weight:bold;'> ", nom, " ", apel, " </span>.");
			insert into bitacoras (informacion, fecha_hora) values (info, now());
        end $
delimiter ;

delimiter $
	create procedure pr_aceptar_rechazar_solicitud(in accion int, in soli int, in usua int)
    begin
		declare total double(10, 2);
		declare situa varchar(1000);
		declare soli_monto int;
		declare soli_usua varchar(50);
		declare soli_situa varchar(1000);
        declare evento varchar(35);
		declare info varchar(5000);
		set situa = (select situacion from solicitudes where id_solicitud in (soli));
        set soli_monto = (select monto from solicitudes where id_solicitud in (soli));
        set soli_usua = concat((select nombre from usuarios where id_usuario in (usua)), 
				" ", (select nombre from usuarios where id_usuario in (usua)));
		if accion = 1 then /*aceptar*/
			set evento = "aceptado";
			update solicitudes set estado = 1 where id_solicitud = soli;
			if (select tipo from solicitudes where id_solicitud in (soli)) = "recaudacion" then 
				set total = (select c.saldo from solicitudes as s inner join cuentas as c on c.id_cuenta = s.id_cuenta where s.id_solicitud in (soli)) +  
						(select monto from solicitudes where id_solicitud in (soli));
            else
				set total = (select c.saldo from solicitudes as s inner join cuentas as c on c.id_cuenta = s.id_cuenta where s.id_solicitud in (soli)) -  
						(select monto from solicitudes where id_solicitud in (soli));
            end if;
			update cuentas set saldo = total where id_cuenta = (select id_cuenta from solicitudes where id_solicitud = soli);
		elseif accion = 0 then /*reactivar*/
			set evento = "reactivado";
			update solicitudes set estado = 0 where id_solicitud = soli;
		elseif accion = 3 then /*eliminar de aceptados*/
			set evento = "eliminado";
			update solicitudes set estado = 0 where id_solicitud = soli;
			if (select tipo from solicitudes where id_solicitud in (soli)) = "recaudacion" then 
				set total = (select c.saldo from solicitudes as s inner join cuentas as c on c.id_cuenta = s.id_cuenta where s.id_solicitud = soli) -  
						(select monto from solicitudes where id_solicitud in (soli));
            else
				set total = (select c.saldo from solicitudes as s inner join cuentas as c on c.id_cuenta = s.id_cuenta where s.id_solicitud = soli) +  
						(select monto from solicitudes where id_solicitud in (soli));
            end if;
			update cuentas set saldo = total where id_cuenta = (select id_cuenta from solicitudes where id_solicitud = soli);
        else /*rechazar*/
			set evento = "rechazado";
			update solicitudes set estado = 2 where id_solicitud = soli;
        end if ;
		set info = concat("Se ha <span style='color:#DF01A5;font-weight:bold;'> ", evento, " </span> la ",
			"<span style='color:#DF01A5;font-weight:bold;'> solicitud </span> #",
			"<span style='color:#DF01A5;font-weight:bold;'> ", soli, "</span>, a quien le fue asignado el monto de: $",
			"<span style='color:#DF01A5;font-weight:bold;'> ", soli_monto, "</span>. Esta solicitud fue reactivada por",
			"<span style='color:#DF01A5;font-weight:bold;'> ", soli_usua, "</span>. La situacion de esta solicitud fue: '",
			"<span style='color:#DF01A5;font-weight:bold;'> ", situa, "</span>'.");
		insert into bitacoras (informacion, fecha_hora) values (info, now());
    end $
delimiter ;

call pr_crear_usuario("Luis Humberto", "Gonzalez Moz", "LuisGM", "lhgonzalezmoz@hotmail.com", "7643 - 3357", 
		"Centro urbano atlacatl (cerca del inframen)", 1, "qweasdzxc", 0);
call pr_crear_afiliacion("Atento S.V", "29 avenida norte (salvador del mundo)", "2250 - 5555", 1); 
call pr_crear_cuentas("4561 6549 8523 4578", 1, 1);
call pr_crear_usuario("Valentina Elizabeth", "Barrios Duran", "VEBD", "barriosduran@gmail.com", "7719 - 5623", 
		"Lourdes color azul (metrocentro galerias)", 5, "cxzdsaewq", 1);
call pr_crear_usuario("Iris Ivette", "Moz Mejia", "iris1999", "irisivette@gmail.com", "7719 - 1193",
		"Mexicanos (colonia buena vista)", 3, "123456789", 1);
call pr_hacer_solicitud(525.44, 0, "El hijo del funcionario dono esta cantidad (andaba de buen humor)", 1, 2);
call pr_hacer_solicitud(35.99, 1, "No tengo dinero para matricular a mi chuchito a clases de manejo", 1, 2);
call pr_aceptar_rechazar_solicitud(1, 1, 1);
call pr_aceptar_rechazar_solicitud(1, 2, 1);

/*
	select * from usuarios;
	select * from cuentas;
	select * from bitacoras;
    select * from solicitudes;
*/