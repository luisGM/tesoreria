package com.teso.control;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.teso.models.Usuarios;
import com.teso.impl.UsuariosDao;
@Controller
public class ControllerLogin {
	@Autowired
	@Qualifier("UsuariosDao")
	private UsuariosDao usuariosDao;
	private Usuarios usuarios;
	private ModelAndView mv;
	@PostConstruct
	public void init () {
		this.limpiar();
	}
	public void limpiar () {
		this.mv = new ModelAndView();
		if (this.usuarios.getIdUsuario() == null) {
			this.usuarios = new Usuarios();
			this.usuarios.setIdUsuario(0);
		}
	}
	@RequestMapping(value = "login", method = RequestMethod.POST)
	public ModelAndView login () {
		this.mv.setViewName("/sources/view/inicio");
		return this.mv;
	}
	@RequestMapping(value = "verificarUsuario", method = RequestMethod.POST)
	public ModelAndView verificarUsuario (@RequestParam("txtEmail") String email, @RequestParam("txtPassword") String password) {
		this.usuarios.setCorreo(email);
		this.usuarios.setContra(password);
		int usuario = this.usuariosDao.logearUsuario(this.usuarios);
		if (usuario == 0) {
			this.mv.addObject("mensaje", "El usuario con el correo " + this.usuarios.getCorreo() + " no existe");
			this.mv.setViewName("/sources/view/inicio");
		} else {
			this.mv.addObject("usuario", this.usuarios.getCorreo());
			this.mv.setViewName("/sources/view/inicio2");
		}
		return this.mv;
	}
}