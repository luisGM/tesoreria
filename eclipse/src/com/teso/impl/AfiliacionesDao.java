package com.teso.impl;
import javax.transaction.Transactional;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.teso.models.Afiliaciones;
import com.teso.utils.AbsFacade;
import com.teso.utils.Dao;
@Repository
@Transactional
public class AfiliacionesDao extends AbsFacade<Afiliaciones> implements Dao<Afiliaciones>{
	@Autowired
	SessionFactory startSession;
	public AfiliacionesDao(SessionFactory sessionFactory) {
		super(Afiliaciones.class);
		this.startSession = sessionFactory;
	}
	@Override
	public SessionFactory getSessionFactory() {
		return startSession;
	}
}