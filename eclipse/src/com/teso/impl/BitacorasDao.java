package com.teso.impl;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.teso.models.Bitacoras;
import com.teso.utils.AbsFacade;
import com.teso.utils.Dao;
public class BitacorasDao extends AbsFacade<Bitacoras> implements Dao<Bitacoras>{
	@Autowired
	SessionFactory startSession;
	public BitacorasDao(SessionFactory sessionFactory) {
		super(Bitacoras.class);
		this.startSession = sessionFactory;
	}
	@Override
	public SessionFactory getSessionFactory() {
		return startSession;
	}
}