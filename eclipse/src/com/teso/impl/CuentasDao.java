package com.teso.impl;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.teso.models.Cuentas;
import com.teso.utils.AbsFacade;
import com.teso.utils.Dao;
public class CuentasDao extends AbsFacade<Cuentas> implements Dao<Cuentas>{
	@Autowired
	SessionFactory startSession;
	public CuentasDao(SessionFactory sessionFactory) {
		super(Cuentas.class);
		this.startSession = sessionFactory;
	}
	@Override
	public SessionFactory getSessionFactory() {
		return startSession;
	}
}