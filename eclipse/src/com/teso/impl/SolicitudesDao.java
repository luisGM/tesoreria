package com.teso.impl;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.teso.models.Solicitudes;
import com.teso.utils.AbsFacade;
import com.teso.utils.Dao;
public class SolicitudesDao extends AbsFacade<Solicitudes> implements Dao<Solicitudes>{
	@Autowired
	SessionFactory startSession;
	public SolicitudesDao(SessionFactory sessionFactory) {
		super(Solicitudes.class);
		this.startSession = sessionFactory;
	}
	@Override
	public SessionFactory getSessionFactory() {
		return startSession;
	}
}