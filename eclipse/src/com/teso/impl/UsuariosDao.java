package com.teso.impl;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;


import com.teso.models.Usuarios;
import com.teso.utils.AbsFacade;
import com.teso.utils.Dao;
public class UsuariosDao extends AbsFacade<Usuarios> implements Dao<Usuarios>{
	@Autowired
	SessionFactory startSession;
	public UsuariosDao(SessionFactory sessionFactory) {
		super(Usuarios.class);
		this.startSession = sessionFactory;
	}
	@Override
	public SessionFactory getSessionFactory() {
		return startSession;
	}
	public int logearUsuario (Usuarios u) {
		int usuario = 0;
		try {
			Query q = startSession.createEntityManager().createNativeQuery("select id_usuario from usuarios where correo = " + u.getCorreo());
			List<Usuarios> lista = new LinkedList<>();
			if (!q.getResultList().isEmpty()) {
				lista = q.getResultList();
				usuario = lista.get(0).getIdUsuario();
			}
		} catch (Exception e) {
			throw e;
		}
		return usuario;
	}
}