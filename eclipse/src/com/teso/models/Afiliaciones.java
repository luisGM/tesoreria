package com.teso.models;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
@Entity
@Table(name = "afiliaciones", catalog = "sistema_tesoreria")
public class Afiliaciones implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private Integer idAfiliacion;
	private Usuarios usuarios;
	private String nombre;
	private String ubicacion;
	private String telefono;
	private String fecha;
	private List<Cuentas> cuentases = new ArrayList<Cuentas>(0);
	public Afiliaciones() {
	}
	public Afiliaciones(Usuarios usuarios, String nombre, String ubicacion, String telefono, String fecha) {
		this.usuarios = usuarios;
		this.nombre = nombre;
		this.ubicacion = ubicacion;
		this.telefono = telefono;
		this.fecha = fecha;
	}
	public Afiliaciones(Usuarios usuarios, String nombre, String ubicacion, String telefono, String fecha,
			List<Cuentas> cuentases) {
		this.usuarios = usuarios;
		this.nombre = nombre;
		this.ubicacion = ubicacion;
		this.telefono = telefono;
		this.fecha = fecha;
		this.cuentases = cuentases;
	}
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id_afiliacion", unique = true, nullable = false)
	public Integer getIdAfiliacion() {
		return this.idAfiliacion;
	}
	public void setIdAfiliacion(Integer idAfiliacion) {
		this.idAfiliacion = idAfiliacion;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_usuario", nullable = false)
	public Usuarios getUsuarios() {
		return this.usuarios;
	}
	public void setUsuarios(Usuarios usuarios) {
		this.usuarios = usuarios;
	}
	@Column(name = "nombre", nullable = false, length = 100)
	public String getNombre() {
		return this.nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	@Column(name = "ubicacion", nullable = false, length = 250)
	public String getUbicacion() {
		return this.ubicacion;
	}
	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}
	@Column(name = "telefono", nullable = false, length = 11)
	public String getTelefono() {
		return this.telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	@Column(name = "fecha", nullable = false, length = 20)
	public String getFecha() {
		return this.fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "afiliaciones")
	public List<Cuentas> getCuentases() {
		return this.cuentases;
	}
	public void setCuentases(List<Cuentas> cuentases) {
		this.cuentases = cuentases;
	}
}