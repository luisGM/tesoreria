package com.teso.models;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "bitacoras", catalog = "sistema_tesoreria")
public class Bitacoras implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private Integer idBitacora;
	private String informacion;
	private String fechaHora;
	public Bitacoras() {
	}
	public Bitacoras(String informacion, String fechaHora) {
		this.informacion = informacion;
		this.fechaHora = fechaHora;
	}
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id_bitacora", unique = true, nullable = false)
	public Integer getIdBitacora() {
		return this.idBitacora;
	}
	public void setIdBitacora(Integer idBitacora) {
		this.idBitacora = idBitacora;
	}
	@Column(name = "informacion", nullable = false, length = 5000)
	public String getInformacion() {
		return this.informacion;
	}
	public void setInformacion(String informacion) {
		this.informacion = informacion;
	}
	@Column(name = "fecha_hora", nullable = false, length = 20)
	public String getFechaHora() {
		return this.fechaHora;
	}
	public void setFechaHora(String fechaHora) {
		this.fechaHora = fechaHora;
	}
}