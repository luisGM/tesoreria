package com.teso.models;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
@Entity
@Table(name = "cuentas", catalog = "sistema_tesoreria")
public class Cuentas implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private Integer idCuenta;
	private Afiliaciones afiliaciones;
	private Usuarios usuarios;
	private String numero;
	private double saldo;
	private List<Solicitudes> solicitudeses = new ArrayList<Solicitudes>(0);
	public Cuentas() {
	}
	public Cuentas(Afiliaciones afiliaciones, Usuarios usuarios, String numero, double saldo) {
		this.afiliaciones = afiliaciones;
		this.usuarios = usuarios;
		this.numero = numero;
		this.saldo = saldo;
	}
	public Cuentas(Afiliaciones afiliaciones, Usuarios usuarios, String numero, double saldo,
			List<Solicitudes> solicitudeses) {
		this.afiliaciones = afiliaciones;
		this.usuarios = usuarios;
		this.numero = numero;
		this.saldo = saldo;
		this.solicitudeses = solicitudeses;
	}
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id_cuenta", unique = true, nullable = false)
	public Integer getIdCuenta() {
		return this.idCuenta;
	}
	public void setIdCuenta(Integer idCuenta) {
		this.idCuenta = idCuenta;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_afiliacion", nullable = false)
	public Afiliaciones getAfiliaciones() {
		return this.afiliaciones;
	}
	public void setAfiliaciones(Afiliaciones afiliaciones) {
		this.afiliaciones = afiliaciones;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_usuario", nullable = false)
	public Usuarios getUsuarios() {
		return this.usuarios;
	}
	public void setUsuarios(Usuarios usuarios) {
		this.usuarios = usuarios;
	}
	@Column(name = "numero", nullable = false, length = 19)
	public String getNumero() {
		return this.numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	@Column(name = "saldo", nullable = false, precision = 10)
	public double getSaldo() {
		return this.saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "cuentas")
	public List<Solicitudes> getSolicitudeses() {
		return this.solicitudeses;
	}
	public void setSolicitudeses(List<Solicitudes> solicitudeses) {
		this.solicitudeses = solicitudeses;
	}
}