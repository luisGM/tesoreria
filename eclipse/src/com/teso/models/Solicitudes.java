package com.teso.models;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name = "solicitudes", catalog = "sistema_tesoreria")
public class Solicitudes implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private Integer idSolicitud;
	private Cuentas cuentas;
	private Usuarios usuarios;
	private double monto;
	private String fecha;
	private String tipo;
	private int estado;
	private String motivo;
	private String situacion;
	public Solicitudes() {
	}
	public Solicitudes(Cuentas cuentas, Usuarios usuarios, double monto, String fecha, String tipo, int estado,
			String motivo, String situacion) {
		this.cuentas = cuentas;
		this.usuarios = usuarios;
		this.monto = monto;
		this.fecha = fecha;
		this.tipo = tipo;
		this.estado = estado;
		this.motivo = motivo;
		this.situacion = situacion;
	}
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id_solicitud", unique = true, nullable = false)
	public Integer getIdSolicitud() {
		return this.idSolicitud;
	}
	public void setIdSolicitud(Integer idSolicitud) {
		this.idSolicitud = idSolicitud;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_cuenta", nullable = false)
	public Cuentas getCuentas() {
		return this.cuentas;
	}
	public void setCuentas(Cuentas cuentas) {
		this.cuentas = cuentas;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_usuario", nullable = false)
	public Usuarios getUsuarios() {
		return this.usuarios;
	}
	public void setUsuarios(Usuarios usuarios) {
		this.usuarios = usuarios;
	}
	@Column(name = "monto", nullable = false, precision = 10)
	public double getMonto() {
		return this.monto;
	}
	public void setMonto(double monto) {
		this.monto = monto;
	}
	@Column(name = "fecha", nullable = false, length = 20)
	public String getFecha() {
		return this.fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	@Column(name = "tipo", nullable = false, length = 15)
	public String getTipo() {
		return this.tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	@Column(name = "estado", nullable = false)
	public int getEstado() {
		return this.estado;
	}
	public void setEstado(int estado) {
		this.estado = estado;
	}
	@Column(name = "motivo", nullable = false, length = 500)
	public String getMotivo() {
		return this.motivo;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	@Column(name = "situacion", nullable = false, length = 1000)
	public String getSituacion() {
		return this.situacion;
	}
	public void setSituacion(String situacion) {
		this.situacion = situacion;
	}
}