package com.teso.models;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
@Entity
@Table(name = "usuarios", catalog = "sistema_tesoreria")
public class Usuarios implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idUsuario;
	private String nombre;
	private String apellido;
	private String usuario;
	private String correo;
	private String telefono;
	private String direccion;
	private int imagen;
	private String contra;
	private int tipo;
	private List<Cuentas> cuentases = new ArrayList<Cuentas>(0);
	private List<Solicitudes> solicitudeses = new ArrayList<Solicitudes>(0);
	private List<Afiliaciones> afiliacioneses = new ArrayList<Afiliaciones>(0);
	public Usuarios() {
	}
	public Usuarios(String nombre, String apellido, String usuario, String correo, String telefono, String direccion,
			int imagen, String contra, int tipo) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.usuario = usuario;
		this.correo = correo;
		this.telefono = telefono;
		this.direccion = direccion;
		this.imagen = imagen;
		this.contra = contra;
		this.tipo = tipo;
	}
	public Usuarios(String nombre, String apellido, String usuario, String correo, String telefono, String direccion,
			int imagen, String contra, int tipo, List<Cuentas> cuentases, List<Solicitudes> solicitudeses,
			List<Afiliaciones> afiliacioneses) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.usuario = usuario;
		this.correo = correo;
		this.telefono = telefono;
		this.direccion = direccion;
		this.imagen = imagen;
		this.contra = contra;
		this.tipo = tipo;
		this.cuentases = cuentases;
		this.solicitudeses = solicitudeses;
		this.afiliacioneses = afiliacioneses;
	}
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id_usuario", unique = true, nullable = false)
	public Integer getIdUsuario() {
		return this.idUsuario;
	}
	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}
	@Column(name = "nombre", nullable = false, length = 25)
	public String getNombre() {
		return this.nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	@Column(name = "apellido", nullable = false, length = 25)
	public String getApellido() {
		return this.apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	@Column(name = "usuario", nullable = false, length = 25)
	public String getUsuario() {
		return this.usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	@Column(name = "correo", nullable = false, length = 50)
	public String getCorreo() {
		return this.correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	@Column(name = "telefono", nullable = false, length = 11)
	public String getTelefono() {
		return this.telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	@Column(name = "direccion", nullable = false, length = 250)
	public String getDireccion() {
		return this.direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	@Column(name = "imagen", nullable = false)
	public int getImagen() {
		return this.imagen;
	}
	public void setImagen(int imagen) {
		this.imagen = imagen;
	}
	@Column(name = "contra", nullable = false, length = 50)
	public String getContra() {
		return this.contra;
	}
	public void setContra(String contra) {
		this.contra = contra;
	}
	@Column(name = "tipo", nullable = false)
	public int getTipo() {
		return this.tipo;
	}
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "usuarios")
	public List<Cuentas> getCuentases() {
		return this.cuentases;
	}
	public void setCuentases(List<Cuentas> cuentases) {
		this.cuentases = cuentases;
	}
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "usuarios")
	public List<Solicitudes> getSolicitudeses() {
		return this.solicitudeses;
	}
	public void setSolicitudeses(List<Solicitudes> solicitudeses) {
		this.solicitudeses = solicitudeses;
	}
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "usuarios")
	public List<Afiliaciones> getAfiliacioneses() {
		return this.afiliacioneses;
	}
	public void setAfiliacioneses(List<Afiliaciones> afiliacioneses) {
		this.afiliacioneses = afiliacioneses;
	}
}