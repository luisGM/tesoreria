package com.teso.utils;
import java.util.List;
import javax.persistence.criteria.CriteriaQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
public abstract class AbsFacade<T> {
	private Class<T> entityClass;
	public AbsFacade(Class<T> entityClass) {
		this.entityClass = entityClass;
	}
	@Autowired
	public abstract SessionFactory getSessionFactory();
	protected Session session;
	@Transactional
	public void crear(T entity) {
		session = getSessionFactory().getCurrentSession();
		session.save(entity);
	}
	@Transactional
	public void editar(T entity) {
		session = getSessionFactory().getCurrentSession();
		session.update(entity);
	}
	@Transactional
	public void eliminar(T entity) {
		session = getSessionFactory().getCurrentSession();
		session.delete(entity);
	}
	@Transactional
	public List<T> listar() {
		session = getSessionFactory().getCurrentSession();
		CriteriaQuery cq = session.getCriteriaBuilder().createQuery();
		cq.select(cq.from(entityClass));
		return session.createQuery(cq).getResultList();
	}
}