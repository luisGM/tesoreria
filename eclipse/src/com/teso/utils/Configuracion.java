package com.teso.utils;
import java.util.Properties;
import javax.sql.DataSource;
import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import com.teso.impl.AfiliacionesDao;
import com.teso.impl.BitacorasDao;
import com.teso.impl.CuentasDao;
import com.teso.impl.SolicitudesDao;
import com.teso.impl.UsuariosDao;
@Configuration
@EnableTransactionManagement(proxyTargetClass = true)
@EnableWebMvc
@ComponentScan({"com.teso"}) 
public class Configuracion {
	@Bean
	public InternalResourceViewResolver 
			vistas() {
		InternalResourceViewResolver resultado = new InternalResourceViewResolver();
		resultado.setPrefix("/");
		resultado.setSuffix(".jsp");
		return resultado;
	}
	@Bean(name = "datasource")
	public DataSource dataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://192.168.101.91:3306/sistema_tesoreria?useSSL=false");
		dataSource.setUsername("kz");
		dataSource.setPassword("kzroot");
		return dataSource;
	}
	@Autowired
	@Bean(name = "sessionFactory")
	public SessionFactory getSessionFactory(DataSource datasource) {
		LocalSessionFactoryBuilder fabrica = new LocalSessionFactoryBuilder(datasource);
		fabrica.scanPackages("com.frt.models");
		fabrica.addProperties(getHibernateProperties());
		return fabrica.buildSessionFactory();
	}
	@Autowired
	@Bean(name = "transactionManager")
	public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory) {
		HibernateTransactionManager tx = new HibernateTransactionManager(sessionFactory);
		return tx;
	}
	private Properties getHibernateProperties() {
		Properties hibernateProperties = new Properties();
		// hibernateProperties.put("hibernate.show_sql", "true");
		hibernateProperties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		return hibernateProperties;
	}
	@Autowired
	@Bean(name = "AfiliacionesDao")
	public AfiliacionesDao afiliacionesDao(SessionFactory sessionFactory) {
		return new AfiliacionesDao(sessionFactory);
	}
	@Autowired
	@Bean(name = "BitacorasDao")
	public BitacorasDao bitacorasDao(SessionFactory sessionFactory) {
		return new BitacorasDao(sessionFactory);
	}
	@Autowired
	@Bean(name = "CuentasDao")
	public CuentasDao cuentasDao(SessionFactory sessionFactory) {
		return new CuentasDao(sessionFactory);
	}
	@Autowired
	@Bean(name = "SolicitudesDao")
	public SolicitudesDao solicitudesDao(SessionFactory sessionFactory) {
		return new SolicitudesDao(sessionFactory);
	}
	@Autowired
	@Bean(name = "UsuariosDao")
	public UsuariosDao usuariosDao(SessionFactory sessionFactory) {
		return new UsuariosDao(sessionFactory);
	}
}