package com.teso.utils;
import java.util.List;
public interface Dao<T> {
	public void crear(T e);
	public List<T> listar();
	public void editar(T e);
	public void eliminar(T e);
}